# -*- coding: utf-8 -*-

'''
LogoServer module
'''

import os
import pathlib
import tarfile
import io

import cherrypy
from cherrypy.lib.static import serve_fileobj


class LogoServer:
    '''
    LogoServer class
    https://www.idlecore.com/logo_server?name=[company_name]
    https://www.idlecore.com/logo_version?name=[company_name]
    
    I can't receive multiple arguments per request.
    Using the regular way receiving parameters, I would have to do type
    checking on the data, which isn't idiomatic in python. Using the * operator
    doesn't work with unicode strings, and using the ** operator requires type
    checking as well. So single parameters it is.
    '''
    
    def __init__(self):
        self.logos = set(os.listdir('images'))
    
    @cherrypy.expose
    def logo_server(self, name=None):
        if not name:
            return
        name = name.lower()
        if name in self.logos:
            with io.BytesIO() as in_memory_file:
                logo_file = tarfile.open(fileobj=in_memory_file, mode='w')
                path = pathlib.Path('images') / name
                logo_file.add(str(path))
                if path.is_symlink():
                    path = path.resolve().relative_to(path.cwd())
                    logo_file.add(str(path))
                logo_file.close()
                return serve_fileobj(fileobj=in_memory_file.getvalue(),
                                     content_type="application/x-download",
                                     disposition="attachment",
                                     name='logo.tar')
        else:
            msg = 'SERVER: Logo name unavailable: {}'
            cherrypy.log(msg.format(repr(name)))
    
    @cherrypy.expose
    def logo_version(self, name=None):
        if not name:
            return
        results = {}
        name = name.lower()
        if name in self.logos:
            path = pathlib.Path('images') / name / 'version'
            with open(str(path)) as version_file:
                return version_file.read().split()
        else:
            msg = 'VERSION: Logo name unavailable: {}'
            cherrypy.log(msg.format(repr(name)))

