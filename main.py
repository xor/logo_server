#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Module for the main program entry point
'''

import cherrypy

import server


def main():
    cherrypy.config.update({'server.socket_host': 'idlecore.com',
                            'server.socket_port': 80,
                            'log.screen': False,
                            'log.access_file': 'cherrypy.access_file',
                            'log.error_file': 'cherrypy.error_file'})
    cherrypy.quickstart(server.LogoServer())


if __name__ == '__main__':
   main()

