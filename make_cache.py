#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Tool to make the client side logo name cache

What is this?
It's a little script that will collect all the names on the images directory
and create a sorted list of those names, that will then be pickled and finally
compressed.

Why do this?
On PassMan, when a user is creating an account, I want to check, in close to
real time, if there is a logo image corresponding to the company or service
the user is inputting in the Service field. Sending a GET to the server each
time the text on the entry changes can be inefficient, so using a local cache
of company and service names let's me make a GET request *only* when the text
on the Service field matches a name I know is on the server.

This is important:
Remember that the cache doesn't need to include all logo names. At first
collecting all names is fine, but if the size of the cache is ever too big,
we can just select the most popular ones. The expected behavior for a client
using this cache is to use it as long as real-time name checking is required,
but if the name isn't found on the cache, after real-time name checking isn't
required anymore, check with the server directly.
'''

import os
import pickle
import bz2


def main():
    # Sorting is used to improve compression.
    dir_names = sorted(os.listdir('images'))
    with bz2.open('logo_name_cache.bz2', 'wb') as logo_name_cache_file:
        # Protocol 4 is used to improve compression as well.
        cached_bytes = pickle.dumps(dir_names, protocol=4)
        logo_name_cache_file.write(cached_bytes)


if __name__ == '__main__':
    main()

